import { Component, HostListener, OnInit } from '@angular/core'
import { AlertController, LoadingController, Platform } from '@ionic/angular'
import { AuthService } from '../../services/auth/auth.service'
import { SessionService } from '../../services/session/session.service'
import { HttpClient } from '@angular/common/http'
import { Game } from '../../interfaces/Game'
import { Session } from '../../interfaces/Session'
import { Router } from '@angular/router'
import { Plugins } from '@capacitor/core'
const  { Storage } = Plugins
import { User } from '../../interfaces/User'

@Component({
    selector: 'app-homepage',
    templateUrl: './homepage.page.html',
    styleUrls: ['./homepage.page.scss'],
})

export class HomepagePage implements OnInit {

    sessions: Session[] = []
    mySessions: Session[] = []
    myHistory: Session[] = []
    user: User = null
    topGames: Game[] = []
    isDesktop = false
    today = Date.now()

    constructor(
        private auth: AuthService,
        private sessionService: SessionService,
        private alertController: AlertController,
        private http: HttpClient,
        private plt: Platform,
        private loadingController: LoadingController,
        private router: Router,
    ) { }

    async ngOnInit() {
        const width = this.plt.width()
        this.toggleTabs(width)

        const hasUsername = await this.checkUsername()

        if (!hasUsername) {
            await this.router.navigateByUrl('welcome')
        }
    }

    async ionViewWillEnter() {
        const loading = await this.loadingController.create()
        await loading.present()

        await Promise.all([this.callCurrentUserInfos(), this.callTopGames(), this.callMySessions(), this.callMyHistory()])
            .then(() => {
                loading.dismiss() })
            .catch(err => console.log(err))
    }

    private async callCurrentUserInfos(): Promise<any> {

       const userAuth: User = await this.auth.getCurrentUser()
       return new Promise (resolve => {
            this.http.post('https://us-central1-gamebridge-app.cloudfunctions.net/getCurrentUserInfos', {
                userUid: userAuth.uid,
                responseType: 'json', headers: {'Access-Control-Allow-Origin': '*'}
            })
                .subscribe(async (data: any) => {
                    await Storage.set({
                        key: 'userLogged',
                        value: JSON.stringify(data)
                    })
                    resolve(data)
                })
        })
    }

    private async callTopGames(): Promise<Game[]> {
        return new Promise (resolve => {
            this.http.get('https://us-central1-gamebridge-app.cloudfunctions.net/getMostPlayedGames', {
                responseType: 'json', headers: {'Access-Control-Allow-Origin': '*'}
            })
                .subscribe((data: any) => {
                    this.topGames = data.json
                    resolve(this.topGames)
                })
        })
    }

    private async callMySessions(): Promise<Session[]> {
        return new Promise ( async resolve => {
            let currentUserUid = ''

            await this.auth.getCurrentUser()
                .then(res => {
                    currentUserUid = res.uid
                })
                .catch(err => console.log(err))

            // Need the current user uid for the following call
            this.http.post('https://us-central1-gamebridge-app.cloudfunctions.net/getFutureSessionsByUserUid', {
                uid: currentUserUid,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'}
            })
                .subscribe((data: Session[]) => {
                    this.mySessions = data
                    resolve(this.mySessions)
                })
        })
    }

    private async callMyHistory(): Promise<Session[]> {
        return new Promise ( async resolve => {
            let currentUserUid = ''

            await this.auth.getCurrentUser()
                .then(res => {
                    currentUserUid = res.uid
                })
                .catch(err => console.log(err))

            // Need the current user uid for the following call
            await this.http.post('https://us-central1-gamebridge-app.cloudfunctions.net/getSessionsByCreatorUid', {
                uid: currentUserUid,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'}
            })
                .subscribe((data: Session[]) => {
                    this.myHistory = data
                    resolve(this.mySessions)
                })
        })
    }

    private async checkUsername(): Promise<unknown> {
        return new Promise ( async resolve => {
            let currentUserUid = ''

            await this.auth.getCurrentUser()
                .then(res => {
                    currentUserUid = res.uid
                })
                .catch(err => console.log(err))

            // Need the current user uid for the following call
            this.http
                .post('https://us-central1-gamebridge-app.cloudfunctions.net/hasUsername', {
                    uid: currentUserUid,
                    responseType: 'json',
                    headers: {'Access-Control-Allow-Origin': '*'}
                })
                .subscribe((data) => {
                    if (data === false) {
                        resolve(data)
                    }
                })
        })
    }

    async openChatSession(session: Session) {
        await this.router.navigateByUrl('tabs/session/chat', { state: {session} })
    }

    toggleTabs(width: number){
        this.isDesktop = width > 768
    }

    @HostListener('window: resize', ['$event'])
    private onResize(event) {
        const newWidth = event.target.innerWidth
        this.toggleTabs(newWidth)
    }
}
