import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { IonicModule } from '@ionic/angular'

import { SessionPlatformPage } from './session-platform.page'

describe('SessionPlatformPage', () => {
  let component: SessionPlatformPage
  let fixture: ComponentFixture<SessionPlatformPage>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionPlatformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents()

    fixture = TestBed.createComponent(SessionPlatformPage)
    component = fixture.componentInstance
    fixture.detectChanges()
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
