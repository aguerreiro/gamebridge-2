import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { Platform } from '../../../interfaces/Platform'
import { Plugins } from '@capacitor/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { LoadingController } from '@ionic/angular'
const  { Storage } = Plugins

@Component({
  selector: 'app-session-platform',
  templateUrl: './session-platform.page.html',
  styleUrls: ['./session-platform.page.scss'],
})

export class SessionPlatformPage implements OnInit {

  platformForm: FormGroup
  allPlatforms: Platform[] = null
  choices: any[] = []

  constructor(
      private fb: FormBuilder,
      private router: Router,
      private http: HttpClient,
      private loadingController: LoadingController,
  ) { }

  ngOnInit() {
    this.platformForm = this.fb.group({
      platformRadio: [null, [Validators.required] ],
    })
  }

  async ionViewWillEnter() {
    const loading = await this.loadingController.create()
    await loading.present()

    await Promise.all<Promise<any>>([this.callAllPlatforms(), this.getSessionFromStorage()])
        .then(() => {
          loading.dismiss()
        })
        .catch(err => console.log(err))
  }

  async saveAndNextStep(platform: Platform): Promise<void> {
    // Get previous data in localStorage
    const sessionStorage = await Storage.get({key: 'session'})
    const session = JSON.parse(sessionStorage.value)

    // Values already set
    const gamename = session.game
    const gameId = session.gameId
    const mode = session.mode
    const goal = session.goal
    // const date = session.date

    // Set the new data

    await Storage.set({
      key: 'session',
      value: JSON.stringify({
        uid: null,
        game: gamename,
        gameId,
        mode,
        goal,
        date: null,
        platform: platform.name,
        description: null,
        createdAt: null,
        expiredAt: null,
        createdBy: null,
        listPlayers: []
      })
    })

    // Debugging
    const sessionMemory = await Storage.get({key: 'session'})
    const sessionAfter = JSON.parse(sessionMemory.value)
    console.log(sessionAfter)

    await this.router.navigateByUrl('tabs/session/date')
  }

  private callAllPlatforms() {
    return new Promise (async resolve => {
      this.http
          .get('https://us-central1-gamebridge-app.cloudfunctions.net/getAllPlatforms ', {
            responseType: 'json', headers: {'Access-Control-Allow-Origin': '*'}
          })
          .subscribe((data: any) => {
            this.allPlatforms = data.json
            resolve(this.allPlatforms)
          })
    })
  }

  private getSessionFromStorage: () => Promise<any[]> = async ()  => {
    return new Promise (async resolve => {
      const sessionStorage = await Storage.get({key: 'session'})
      const session = JSON.parse(sessionStorage.value)

      this.choices.push(session.game, session.mode, session.goal)

      resolve(this.choices)
    })
  }
}
