import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { SessionAvailablePage } from './session-available.page'

const routes: Routes = [
  {
    path: '',
    component: SessionAvailablePage
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionAvailablePageRoutingModule {}
