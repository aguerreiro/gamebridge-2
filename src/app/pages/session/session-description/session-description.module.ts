import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'

import { IonicModule } from '@ionic/angular'

import { SessionDescriptionPageRoutingModule } from './session-description-routing.module'

import { SessionDescriptionPage } from './session-description.page'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SessionDescriptionPageRoutingModule,
        ReactiveFormsModule
    ],
  declarations: [SessionDescriptionPage]
})
export class SessionDescriptionPageModule {}
