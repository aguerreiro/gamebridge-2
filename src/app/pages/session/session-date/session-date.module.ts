import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'

import { IonicModule } from '@ionic/angular'

import { SessionDatePageRoutingModule } from './session-date-routing.module'

import { SessionDatePage } from './session-date.page'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SessionDatePageRoutingModule,
        ReactiveFormsModule
    ],
  declarations: [SessionDatePage]
})
export class SessionDatePageModule {}
