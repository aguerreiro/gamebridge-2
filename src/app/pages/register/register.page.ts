import { Component, OnInit } from '@angular/core'
import { LoadingController, AlertController } from '@ionic/angular'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AuthService } from '../../services/auth/auth.service'
import { Router } from '@angular/router'
import { MustMatch } from '../../_helpers/must-match.validator'

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})

export class RegisterPage implements OnInit {

  registerForm: FormGroup
  showDetails = false
  passwordsNotMatch = null

  constructor(
      private fb: FormBuilder,
      private auth: AuthService,
      private loadingController: LoadingController,
      private alertController: AlertController,
      private router: Router
  ) {}


  ngOnInit() {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    })
  }

  async showAgreement() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Accepter conditions',
      message: 'En vous inscrivant, vous acceptez les <u>Conditions générales d\'utilisation</u> ainsi que les <u>règles de confidentialité</u>',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Terms not accepted')
          }
        }, {
          text: 'J\'accepte',
          handler: () => {
            this.signUp()
          }
        }
      ]
    })

    await alert.present()
  }

  async signUp() {
    const loading = await this.loadingController.create()
    await loading.present()
    console.log(this.registerForm.value)

    this.auth
        .emailSignup(this.registerForm.value).then((res) => {
          console.log(res)
          loading.dismiss()
          this.router.navigateByUrl('/welcome')
        },
        async (err) => {
          console.log('Error: ' + err)
          const alert = await this.alertController.create({
            header: 'Erreur lors de l\'i inscription',
            message: err.message,
            buttons: ['OK'],
          })
          await loading.dismiss()
          await alert.present()
        }
    )
  }

  get email() {
    return this.registerForm.get('email')
  }

  get password() {
    return this.registerForm.get('password')
  }
}
