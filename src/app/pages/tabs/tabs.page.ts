import { Component, OnInit } from '@angular/core'
import { ScreensizeService } from '../../services/screensize/screensize.service'

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})

export class TabsPage implements OnInit {

  isDesktop: boolean

  constructor(
      private screensizeService: ScreensizeService
  ) {
    this.screensizeService.isDesktopView().subscribe(isDesktop => {
      if (this.isDesktop && !this.isDesktop){
        window.location.reload()
      }
      this.isDesktop = isDesktop
    })
  }

  ngOnInit() { }
}
