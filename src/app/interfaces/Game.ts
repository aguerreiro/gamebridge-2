import {Gamemode} from './Gamemode'

export interface Game {
    id: string,
    name: string,
    gamemodes: Gamemode[],
    platforms: string[],
    count: number,
    image: string,
}
