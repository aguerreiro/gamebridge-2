export interface Platform{
    id: string,
    name: string,
    image: string,
    priorityOrder: number,
}
