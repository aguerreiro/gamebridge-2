import { Injectable } from '@angular/core'
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore'
import { AngularFireAuth } from '@angular/fire/auth'
import { AuthService } from '../auth/auth.service'
import { Session } from '../../interfaces/Session'



@Injectable({
  providedIn: 'root'
})

export class SessionService {

  constructor(
      private afAuth: AngularFireAuth,
      private auth: AuthService,
      private afs: AngularFirestore
  ) { }

  /**
   *
   * @param session
   * @param sessionJSON
   * @private
   *
   * This function will:
   * 1° - Check and return false if the user who's creating the session is already the creator of the compared session in DB
   * 2° - Check the platform, game and mode, if true it means there's already a session with the same specifications.
   */
  private static sessionAlreadyExists(session, sessionJSON): boolean{
    return session.platform === sessionJSON.platform && session.game === sessionJSON.game && session.mode === sessionJSON.mode
  }

  private static userAlreadyInThisSession(session, sessionJSON): boolean {
    return session.listPlayers.includes(sessionJSON.createdBy)
  }

  private static createSession(): void {
    console.log('Session created')
  }

  /**
   * Create a session
   * @param sessionJSON
   * @private
   */
  private createSession(sessionJSON){
    // Generated id for the session
    const uid = this.afs.createId()
    const sessionRef: AngularFirestoreDocument<Session> = this.afs.doc(`sessions/${uid}`)

    const data = {
      uid,
      game: sessionJSON.game,
      gameId: sessionJSON.gameId,
      mode: sessionJSON.mode,
      platform: sessionJSON.platform,
      goal: sessionJSON.goal,
      date: sessionJSON.date,
      description: sessionJSON.description,
      createdAt: sessionJSON.createdAt,
      expiredAt: sessionJSON.expiredAt,
      createdBy: sessionJSON.createdBy,
      listPlayers: sessionJSON.listPlayers,
    }

    return sessionRef.set(data, { merge: true })
  }

  /**
   * This function will:
   * 1° - Check if the database is empty, in this case, the session will be created ==> return 0
   * 2° - Will loop inside session in DB and compare with the in-waiting session.
   *      If a similar session already exists, it will show the list of
   *      available sessions ==> return 1
   * 3° - It will also check if the user has ready created a similar session ==> return 1
   * 4° - Otherwise it will create the session ==> return 2
   * @param sessionJSON
   * @return 0 if database is empty // 1 if session already exists // 2 if the session is unique and is created
   */
  hasSimilar(sessionJSON: Session): Promise<number> {
    let exists = false

    return this.getSessions().then((sessions: Session[]) => {
      // Check empty DB
      if (sessions.length < 1){
        this.createSession(sessionJSON).then(r => console.log('Empty DB'))
        return 0
      }

      // Check if exists
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < sessions.length; i++) {
        if (SessionService.sessionAlreadyExists(sessions[i], sessionJSON)) {
          exists = true
        }
        // if(SessionService.userAlreadyInThisSession(sessions[i], sessionJSON)) {
        //   exists=true;
        // }
      }

      if (exists) {
        return 1
      } else {
        this.createSession(sessionJSON).then(r => console.log('Unique session'))
        return 2
      }
    })
  }

  /**
   * Return all sessions
   */
  async getSessions(): Promise<Session[]> {
    // Get all sessions where current user IS NOT in
    return this.afs.collection('sessions').get().toPromise()
        .then((snapshot) => {
          return snapshot.docs.map((doc) => {
            return doc.data() as Session
          })
        })
  }

  /**
   * Return all sessions joined by a specific user - by uid
   * @param userUid
   */
  getSessionsByUid(userUid: string): Promise<Session[]> {

    return this.afs.collection('sessions', ref => ref.where('listPlayers', 'array-contains', userUid)).get().toPromise()
        .then((snapshot) => {
          return snapshot.docs.map((doc) => {
            return doc.data() as Session
          })
        })
  }

  /**
   * Add a player to a session
   * @param session, userUid
   * @param userUid
   */
  addPlayer(session: Session, userUid: string): Promise<void>{

    // Get current session id
    const uid = session.uid
    const sessionRef: AngularFirestoreDocument<Session> = this.afs.doc(`sessions/${uid}`)

    // Add player uid to the list of players
    session.listPlayers.push(userUid)

    const data = {
      uid,
      game: session.game,
      gameId: session.gameId,
      mode: session.mode,
      goal: session.goal,
      platform: session.platform,
      date: session.date,
      description: session.description,
      createdAt: session.createdAt,
      expiredAt: session.expiredAt,
      listPlayers: session.listPlayers,
      createdBy: session.createdBy
    }

    return sessionRef.set(data, { merge: true })
  }


}
