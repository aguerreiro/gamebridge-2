import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import * as cors from 'cors'


admin.initializeApp()

/**
 * @description Function to test that at least this function works
 */
export const helloWorld = functions.https.onRequest((req, res) => {
    console.log('Hello World !')
    functions.logger.info('Hello', { structuredData: true })
    res.send('Hello World !')
})

/**
 * @description Get most searched games (10 first max.)
 *              From the most searched to the less searched
 */
export const getMostPlayedGames = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()

        const collecRef = db.collection('games').orderBy('count', 'desc').limit(10)
        const arrayOfGames: any[] = []

        collecRef.get().then((snapshot) => {
            snapshot.docs.map(doc => {
                arrayOfGames.push(doc.data())
            })
            functions.logger.info('Getting games in order !', { structuredData: true })
            res.setHeader('Access-Control-Allow-Origin', '*')
            res.send({json: arrayOfGames}).status(200)
        }).catch((err) => {
            res.send({text: err})
        })
    })
})

/**
 * @description Get all the games from the DB, as simple as that
 */
export const getAllGames = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const collecRef = db.collection('games')
        const arrayOfGames: any[] = []

        collecRef.get().then((snapshot) => {
            snapshot.docs.map(doc => {
                arrayOfGames.push(doc.data())
            })
            functions.logger.info('Getting all games', { structuredData: true })
            res.setHeader('Access-Control-Allow-Origin', '*')
            res.send({json: arrayOfGames}).status(200)
        }).catch((err) => {
            res.send({text: err})
        })
    })
})

/**
 * @description Get all the games from the DB, as simple as that
 */
export const getAllPlatforms = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const collecRef = db.collection('platforms').orderBy('priorityOrder')
        const arrayOfPlatforms: any[] = []

        collecRef.get().then((snapshot) => {
            snapshot.docs.map(doc => {
                arrayOfPlatforms.push(doc.data())
            })
            functions.logger.info('Getting all games', { structuredData: true })
            res.setHeader('Access-Control-Allow-Origin', '*')
            res.send({json: arrayOfPlatforms}).status(200)
        }).catch((err) => {
            res.send({text: err})
        })
    })
})

/**
 * @description Get all the goals
 */
export const getAllGoals = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const collecRef = db.collection('goals').orderBy('priorityOrder')
        const arrayOfGoals: any[] = []

        collecRef.get().then((snapshot) => {
            snapshot.docs.map(doc => {
                arrayOfGoals.push(doc.data())
            })

            functions.logger.info('Getting all goals', { structuredData: true })
            res.setHeader('Access-Control-Allow-Origin', '*')
            res.send({json: arrayOfGoals}).status(200)
        }).catch((err) => {
            res.send({text: err})
        })
    })
})

/**
 * @description Get all the Communication ways
 */
export const getAllCommunication = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const collecRef = db.collection('communication').orderBy('priorityOrder')
        const arrayOfComm: any[] = []

        collecRef.get().then((snapshot) => {
            snapshot.docs.map(doc => {
                arrayOfComm.push(doc.data())
            })

            functions.logger.info('Getting all communication ways', { structuredData: true })
            res.setHeader('Access-Control-Allow-Origin', '*')
            res.send({json: arrayOfComm}).status(200)
        }).catch((err) => {
            res.send({text: err})
        })
    })
})

/**
 * @description Get all the Sessions
 */
export const getAllSessions: any = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const collecRef = db.collection('sessions')
        const arrayOfSessions: any[] = []

        collecRef.get().then((snapshot) => {
            snapshot.docs.map(doc => {
                arrayOfSessions.push(doc.data())
            })

            functions.logger.info('Getting all communication ways', { structuredData: true })
            res.setHeader('Access-Control-Allow-Origin', '*')
            res.send({json: arrayOfSessions}).status(200)
        }).catch((err) => {
            res.send({text: err})
        })
    })
})

/**
 * @description Get all the Levels ways
 */
export const getAllLevels = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const collecRef = db.collection('levels').orderBy('priorityOrder')
        const arrayOfLevels: any[] = []

        collecRef.get().then((snapshot) => {
            snapshot.docs.map(doc => {
                arrayOfLevels.push(doc.data())
            })

            functions.logger.info('Getting all Levels ways', { structuredData: true })
            res.setHeader('Access-Control-Allow-Origin', '*')
            res.send({json: arrayOfLevels}).status(200)
        }).catch((err) => {
            res.send({text: err})
        })
    })
})

/**
 * @param uid: string
 * @description Get all the Sessions where the user is
 *              Check in the listOfPlayers if user is in it
 */
export const getSessionsByUserUid = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const sessionsRef = db.collection('sessions')
        const uid: string = req.body.uid
        const listSessions: any[] = []

        sessionsRef
            .where('listPlayers', 'array-contains', uid)
            .get()
            .then(snap => {
                snap.forEach((doc) => {
                    listSessions.push(doc.data())
                })

                functions.logger.info('Sessions by userUid done', { structuredData: true })
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(listSessions).status(200)
            })
            .catch(error => console.log(error))
    })
})

/**
 * @param uid: string
 * @description Get all the future sessions where the user is in and > to the actual datetime
 *              Check in the listOfPlayers if user is in it
 *              Index created on Firebase for this function
 */
export const getFutureSessionsByUserUid = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const sessionsRef = db.collection('sessions')
        const uid: string = req.body.uid
        const listSessions: any[] = []

        sessionsRef
            .where('listPlayers', 'array-contains', uid)
            .where('expiredAt', '>', Date.now())
            .get()
            .then(snap => {
                snap.forEach((doc) => {
                    listSessions.push(doc.data())
                    functions.logger.info(Date.now() + '  ' + doc.data().date, { structuredData: true })
                })

                functions.logger.info('Sessions by userUid done', { structuredData: true })
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(listSessions).status(200)
            })
            .catch(error => console.log(error))
    })
})

/**
 * 1° - Create the session
 * 2° - Increment the counting of the game
 *
 * @description Create a Session by using the params
 * @param session: Session
 */
export const createSession = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        // Generate an UID
        const key: string | null = admin.database().ref().push().key
        const sessionRef = db.collection('sessions')

        if (req.body.description.description === undefined) {
            req.body.description.description = null
        }

        const timestamp = admin.firestore.FieldValue.serverTimestamp()

        const data = {
            uid: key,
            game: req.body.game,
            gameId: req.body.gameId,
            mode: req.body.mode,
            goal: req.body.goal,
            platform: req.body.platform,
            date: req.body.date,
            description: req.body.description.description,
            createdAt: timestamp,
            expiredAt: req.body.expiredAt,
            createdBy: req.body.createdBy,
            listPlayers: req.body.listPlayers
        }

        if (key !== null) {
            sessionRef.doc(key)
                .create(data)
                .then(() => {
                    console.log('Session created successfully !')
                })
                .then(() => {
                    // Increment game count
                    const gameDoc = db.collection('games').doc(req.body.gameId)

                    gameDoc.update({
                        count: admin.firestore.FieldValue.increment(1)
                    })
                        .then(() => {
                            res.setHeader('Access-Control-Allow-Origin', '*')
                            res.send({msg: 'Game incremented properly'}).status(200)
                        })
                        .catch((err) => {
                            functions.logger.info('Error in incrementing count of game: ' + err, {structuredData: true})
                            res.setHeader('Access-Control-Allow-Origin', '*')
                            res.send(err).status(500)
                        })
                })
                .catch(error => {
                    functions.logger.info('Error in creating the session: ' + error, {structuredData: true})
                    res.setHeader('Access-Control-Allow-Origin', '*')
                    res.send(error).status(500)
                })
        }
    })
})

/**
 * @param session: Session, userUid: string
 * @description When a user creates a Session, this will be also saved in history
 *              Will be pushed in the array "listSessions" in the User collection
 */
export const addSessionsToUserHistory = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const userUid: string = req.body.userUid
        const sessionUid: string = req.body.sessionUid

        const db = admin.firestore()
        const collRef = db.collection('users').doc(userUid).collection('listSessions')

        const data = {
            uid: req.body.sessionUid,
            game: req.body.game,
            gameId: req.body.gameId,
            mode: req.body.mode,
            goal: req.body.goal,
            date: req.body.date,
            platform: req.body.platform,
            description: req.body.description,
            expiredAt: req.body.expiredAt,
            createdBy: req.body.createdBy,
            listPlayers: req.body.listPlayers,
            createdAt: req.body.createdBy
        }

        collRef.doc(sessionUid).set(data, {merge: true})
            .then(() => {
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send({msg: 'Data sent properly'}).status(200)
            })
            .catch(error => {
                functions.logger.info('Error in adding session to user sessions list : ' + error, {structuredData: true})
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(error).status(200)
            })
    })
})

/**
 * @description Get all sessions that have the same: Game, Mode and Goal
 * @param game: Game, mode: Mode, goal: Goal
 */
export const getSessionsByPreferences = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {

        if (
            req.body.game === undefined ||
            req.body.mode === undefined ||
            req.body.goal === undefined
        ) {
            res.setHeader('Access-Control-Allow-Origin', '*')
            res.send({msg: 'Body parameters are missing. Game, mode, goal, userUid are necessary.'})
        }

        const db = admin.firestore()
        const sessionRef = db.collection('sessions')
        const sessionsList: any[] = []

        sessionRef
            .where('game', '==', req.body.game)
            .where('mode', '==', req.body.mode)
            .where('goal', '==', req.body.goal)
            .get()
            .then((snap) => {
                snap.docs.map(element => {
                    sessionsList.push(element.data())
                })
                functions.logger.info('Getting session by preferences OK ', {structuredData: true})
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(sessionsList).status(200)
            })
            .catch((error) => {
                functions.logger.info('Error getting sessions by preferences: ' + error, {structuredData: true})
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(error).status(200)
            })
    })
})

/**
 * @description Push a new user to the session list of players
 * @param playerToAdd: string
 * @param sessionUid: string
 */
export const addPlayerToSession = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const sessionUid: string = req.body.sessionUid
        const playerToAdd: string = req.body.playerToAdd
        const sessionDoc = db.collection('sessions').doc(sessionUid)

        sessionDoc.update({
            listPlayers: admin.firestore.FieldValue.arrayUnion(playerToAdd)
        })
            .then(() => {
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send({msg: 'Player added to session'})
            })
            .catch((err) => {
                functions.logger.info('Error in adding player to the session: ' + err, {structuredData: true})
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(err).status(200)
            })
    })
})

/**
 * @description Remove a given user to the session list of players
 * @param playerToRemove: string, sessionUid: string
 */
export const removePlayerFromSession = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const sessionUid = req.body.sessionUid
        const playerToRemove = req.body.playerToRemove
        const sessionDoc = db.collection('sessions').doc(sessionUid)


        sessionDoc.update({
            listPlayers: admin.firestore.FieldValue.arrayRemove(playerToRemove)
        })
            .then(() => {
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send({msg: 'Player removed from session'}).status(200)
            })
            .catch((err) => {
                functions.logger.info('Error in removing player from the session: ' + err, {structuredData: true})
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(err).status(200)
            })
    })
})

/**
 * @param uid: string
 * @description Get all the Sessions by the createdBy input
 */
export const getSessionsByCreatorUid = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const sessionsRef = db.collection('sessions')
        const uid: string = req.body.uid
        const listSessions: any[] = []

        sessionsRef
            .where('createdBy', '==', uid)
            .get()
            .then(snap => {
                snap.forEach((doc) => {
                    listSessions.push(doc.data())
                })

                functions.logger.info('Sessions by createdBy user done', { structuredData: true })
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(listSessions).status(200)
            })
            .catch(err => {
                res.send(err).status(200)
            })
    })
})

/**
 * @description Push a new user to the session list of players
 * @param playerUid: string
 * @param username: string
 */
export const updateUsername = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const username: string = req.body.username
        const playerUid: string = req.body.playerUid
        const userDoc = db.collection('users').doc(playerUid)

        userDoc.update({
            username
        })
            .then(() => {
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send({msg: 'Username updated !'}).status(200)
            })
            .catch((err) => {
                functions.logger.info('Error in updating username: ' + err, {structuredData: true})
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(err).status(200)
            })
    })
})

/**
 * @description Check if the user as a username in the database
 * @param playerUid: string
 */
export const hasUsername = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const db = admin.firestore()
        const uid: string = req.body.uid
        const userDoc = db.collection('users').doc(uid)

        userDoc
            .get()
            .then(doc => {
                if (doc.exists) {
                    const user = doc.data()

                    if (user !== undefined && user.username !== undefined) {
                        if (user.username !== null && user.username.length > 0) {
                            res.setHeader('Access-Control-Allow-Origin', '*')
                            res.send(true).status(200)
                        } else {
                            res.setHeader('Access-Control-Allow-Origin', '*')
                            res.send(false).status(200)
                        }
                    } else {
                        res.setHeader('Access-Control-Allow-Origin', '*')
                        res.send(false).status(200)
                    }
                }
            })
            .catch(err => {
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(err).status(200)
            })
    })
})

/**
 * @param session: Session, userUid: string
 * @description When a user creates a Session, this will be also saved in history
 *              Will be pushed in the array "listSessions" in the User collection
 */
export const addChatMessage = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const userUid: string = req.body.userUid
        const sessionUid: string = req.body.sessionUid
        const msg: string = req.body.msg
        const username: string = req.body.username

        const db = admin.firestore()
        const msgRef = db.collection('sessions').doc(sessionUid).collection('messages')

        // Generate uid or every message
        const uid: string | null = admin.database().ref().push().key

        const data = {
            msgUid: uid,
            msg,
            createdAt: admin.firestore.FieldValue.serverTimestamp(),
            createdBy: userUid,
            createdByUsername: username
        }

        if (uid) {
            msgRef.doc(uid)
                .create(data)
                .then(() => {
                    console.log('Message sent')
                    functions.logger.info('Message sent properly ! ', {structuredData: true})
                    res.setHeader('Access-Control-Allow-Origin', '*')
                    res.send({msg: 'Message sent !'}).status(200)
                })
                .catch((err) => {
                    functions.logger.info('Error in send the message' + err, {structuredData: true})
                    res.setHeader('Access-Control-Allow-Origin', '*')
                    res.send(err).status(500)
                })
        }
    })
})

/**
 * @param userUid: string
 * @description Get the infos from user Firestore data (necessary for the chat) to get pseudo
 */
export const getCurrentUserInfos = functions.https.onRequest((req, res) => {
    cors({origin: true})(req, res, () => {
        const userUid: string = req.body.userUid

        const db = admin.firestore()
        const userRef = db.collection('users').doc(userUid)

        userRef.get()
            .then((doc) => {
                if (doc.exists) {
                    res.setHeader('Access-Control-Allow-Origin', '*')
                    res.send(doc.data()).status(200)
                } else {
                    res.setHeader('Access-Control-Allow-Origin', '*')
                    res.send({msg: 'No document find with this uid'}).status(200)
                }
            })
            .catch(err => {
                res.setHeader('Access-Control-Allow-Origin', '*')
                res.send(err).status(200)
            })
    })
})






















